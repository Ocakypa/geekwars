﻿using System;
using System.Media;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Media;
using System.Windows.Threading;

namespace GeekWars
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        SoundPlayer player = new SoundPlayer();
        DateTime ButtonPressed;
        bool AllowPress;
        DispatcherTimer UITimer;
        public bool IsStarted;
        Thread checkKeyThread;
        [DllImport("user32.dll")]
        static extern short GetAsyncKeyState(int vk);

        public MainWindow()
        {
            player.Stream = Properties.Resources.Button;

            InitializeComponent();
            IsStarted = true;
            checkKeyThread = new Thread(CheckKeyFunction);
            checkKeyThread.Start();
            InitUITimer();
            this.Closing += MainWindow_Closing;
        }

        private void MainWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            checkKeyThread.Abort();
        }

        public void InitUITimer()
        {
            UITimer = new DispatcherTimer();
            UITimer.Interval = TimeSpan.FromMilliseconds(100);
            UITimer.Tick += UITimer_Tick;
            UITimer.Start();
        }

        private void UITimer_Tick(object sender, EventArgs e)
        {
            if(ButtonPressed.AddSeconds(2)<DateTime.Now)
            {
                TeamColor.Background = Brushes.Black;
                AllowPress = true;
            }   
        }
        /// <summary>
        /// https://msdn.microsoft.com/en-us/library/windows/desktop/dd375731(v=vs.85).aspx
        /// </summary>
        void CheckKeyFunction()
        {
            while (IsStarted)
            {
                if (GetAsyncKeyState(0x30) != 0)  //0
                {
                    if (AllowPress)
                    {
                        AllowPress = false;
                        Dispatcher.BeginInvoke((Action)(() =>
                    {
                        TeamColor.Background = Brushes.Green;
                        ButtonPressed = DateTime.Now;
                        player.Play();
                    }));
                    }
                }
                if (GetAsyncKeyState(0x32) != 0)  //2
                {
                    if (AllowPress)
                    {
                        AllowPress = false;
                        Dispatcher.BeginInvoke((Action)(() =>
                    {
                        TeamColor.Background = Brushes.Red;
                        ButtonPressed = DateTime.Now;
                        player.Play();
                    }));
                    }
                }
                if (GetAsyncKeyState(0x33) != 0)  //3
                {
                    if (AllowPress)
                    {
                        AllowPress = false;
                        Dispatcher.BeginInvoke((Action)(() =>
                    {
                        TeamColor.Background = Brushes.Yellow;
                        ButtonPressed = DateTime.Now;
                        player.Play();

                    }));
                    }
                }
                if (GetAsyncKeyState(0x34) != 0)  //4
                {
                    if (AllowPress)
                    {
                        AllowPress = false;
                        Dispatcher.BeginInvoke((Action)(() =>
                        {
                            TeamColor.Background = Brushes.Blue;
                            ButtonPressed = DateTime.Now;
                            player.Play();
                        }));
                    }
                }

                if (GetAsyncKeyState(0x1B) != 0)
                {
                    Environment.Exit(0);
                }
            }
        }

    }
}
